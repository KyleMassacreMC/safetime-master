<?php

namespace Api\Requests;

use Dingo\Api\Http\FormRequest;

class UserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|confirmed|min:4|max:20',
            'phone' => 'required|phone:AUTO,US|unique:users',
            'country' => 'required|required_with:phone'
        ];
    }

    public function messages()
    {
        return [
            'name.required' => 'You must provide a name',
            'name.max' => 'Your name must not be larder than :max characters',
            'email.required' => 'You must provide an email address',
            'email.email' => 'You must provide a valid email address',
            'email.max' => 'Your email address must not be greater than :max characters',
            'email.unique' => 'It seems that this email address is in use',
            'password.required' => 'You must provide a password',
            'password.confirmed' => 'Your passwords do not match',
            'password.min' => 'Your password must be greater than :min characters',
            'password.max' => 'Your password must be greater than :max characters',
            'phone.required' => 'You must provide a valid phone number [code 1]',
            'phone.phone' => 'You must provide a valid phone number [code2]',
            'phone.unique' => 'You must provide a valid phone number [code3]',
            'country.required' => 'You must provide a country',
            'country.required_with' => 'Your country and phone number doesn\'t match'

        ];
    }
}
