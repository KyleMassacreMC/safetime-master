<?php

use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
        	'name' => 'KyleEllis',
        	'email' => 'admin@example.com',
        	'password' => bcrypt('kyleellis'),
            'phone' => '+19099101681',
            'street' => '3859 Salt River Lane Perris',
            'state' => 'CA',
            'country' => 'US'
    	]);
    }
}
